#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define TAILLE_TAMPON 1024

int main(int argc, char * argv[]) {
    FILE *f;
    char tampon[TAILLE_TAMPON];

    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }

    printf("EUID = %d\tEGID = %d\n", getuid(), getgid());
    printf("RUID = %d\tRGID = %d\n",geteuid(), getegid());
    
    f = fopen(argv[1], "r");
    if (f == NULL) {
        perror("Canot open file\n");
        exit(EXIT_FAILURE);
    }

    while (fscanf(f, "%s", tampon) != EOF) {
        printf("%s ", tampon);
    }
    printf("\n");

    fclose(f);
    return 0;
    
}