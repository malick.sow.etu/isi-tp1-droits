# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Sow, Malick, email: malick.sow.etu@univ-lille.fr

- Ndiaye, El hadji Malick, email: malickpro4@gmail.com

## Question 1

sudo addgroup ubuntu && sudo adduser --ingroup ubuntu toto

Le processus ne peut pas écrire sur le fichier car l'EUID du processus est même que celui du proprietaire du fichier, et le fichier ne donne pas droit en ecriture (w) à son proprietaire. 

## Question 2

le caractere x pour un répertoire indique qu'on peur parcourrir ce repertoire.

l'utlusateur toto ne peut pas entré dans le répertoire mycdir car primo EUID de toto n'est pas identique à celui de mydir et ensuite EGID de toto est identique à celle de mydir, mais parcontre mydir ne donne pas le droit (x) à son groupe. du coup on obtient (Permission denied).

En tapant la commande ls -al on arrive à lister le contenun du dossier car mydir donne le droit (r) à son groupe, parcontre en l'absence du (x) une Permission denied s'affiche après car on a pas droit de parcourir ce repertoire.

## Question 3

Les différents ids sont :  
EUID = 1001	EGID = 1002  
RUID = 1001	RGID = 1002

L'utilisateur toto n'arrive pas a ouvrir le fichier mydir/data.txt en lecture.

En activant le flag set-user-id, on obtient :
EUID = 1001	EGID = 1002
RUID = 1001	RGID = 1002
Premier tp en securité informatique 

On remarque enfin le processus arrive à ouvrire le fichier mydir/data.txt en lecture.

## Question 4

Les différents ids sont :  
EUID  1001  
EGID  1002

## Question 5

chfn permet de modifier les informations associés à un utilisateur.
`
$ ls -al /usr/bin/chfn  
-rwsr-xr-x 1 root root 72712 Jul 14  2021 /usr/bin/chfn

r: l'accès en lecture pour le super utilisateur root, et le groupe root  
w: l'accès en ecriture seulement pour le super utilisateur  
x: droit de l'exécuter pour tous le monde  
S: donne des privilèges auw autres qui execute le fichier, comme si c'était root 

## Question 6

Les mots de pass sont stockés dans le fichier /etc/gshadow

Car /etc/gshadow est securisé, seul root a accés en lecture contrairement à /etc/passwd qui est lisible par tous.


## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








